package des;

public class Block64 {
	public byte[] data;
	
	public Block64(){
		data = new byte[8];
	}
        public Block64(byte[] p){
            data = p;
        }
	
	@Override
	public String toString(){
		return String.format("%02x %02x %02x %02x %02x %02x %02x %02x",
				data[0], data[1], data[2], data[3], 
				data[4], data[5], data[6], data[7]);
	}
}
