package des;

import java.util.Arrays;

/**
 * @author Kolokolov, 2018
 */
public class DES {

	public static void main(String[] args) {
		MainFrame inst = new MainFrame();
		inst.setVisible(true);
	}

	public static Block64 encrypt(Block64 block, Block56 key) {
		initPermutate(block);
		Block32 hb1, hb2;
		hb1 = new Block32();
		hb2 = new Block32();
		split(block, hb1, hb2);
		Block48[] keys = calculateKeys();
		for (int i = 0; i < 16; i++) {
			mix(hb1, hb2, keys[i]);
			if (i != 15) {
				swap(hb1, hb2);
			}
		}
		combine(block, hb1, hb2);
		finalPermutate(block);
		return block;
	}

	private static void initPermutate(Block64 block) {
		block.data = permutate(block.data, initailPermutationTable);
	}

	private static void finalPermutate(Block64 block) {
		block.data = permutate(block.data, finalPermutationTable);
	}

	private static byte[] permutate(byte[] data, byte[] table) {
		byte[] result = new byte[table.length / 8];
		int bit1, byte1, byte2, byte2i, s1, s2;
		for (int i = 0; i < table.length; i++) {
			bit1 = table[i];
			bit1--;
			byte1 = data[bit1 / 8];
			s1 = 7 - (bit1 % 8);
			byte1 >>= s1;
			byte1 &= 0x01;
			byte2i = i / 8;
			s2 = 7 - (i % 8);
			byte2 = byte1 << s2;
			result[byte2i] |= byte2;
		}
		return result;
	}

	private static void split(Block64 src, Block32 dst1, Block32 dst2) {
		if ((src == null) || (dst1 == null) || (dst2 == null)) {
			throw new IllegalArgumentException();
		}
		dst1.data = Arrays.copyOfRange(src.data, 0, 4);
		dst2.data = Arrays.copyOfRange(src.data, 4, 8);
	}

	private static void combine(Block64 dst, Block32 src1, Block32 src2) {
		if ((src1 == null) || (src2 == null) || (dst == null)) {
			throw new IllegalArgumentException();
		}
		int i1 = 0;
		for (int i0 = 0; i0 < 4; i0++, i1++) {
			dst.data[i1] = src1.data[i0];
		}
		for (int i0 = 0; i0 < 4; i0++, i1++) {
			dst.data[i1] = src2.data[i0];
		}
	}

	private static void swap(Block32 p1, Block32 p2) {
		byte[] t = p1.data;
		p1.data = p2.data;
		p2.data = t;
	}

	private static Block48[] calculateKeys() {
		Block48 res[] = new Block48[16];
		// to do
		return res;
	}

	private static void mix(Block32 hbL, Block32 hbR, Block48 key) {
		xorBlock(hbL, function(hbR, key));
	}

	private static Block32 function(Block32 src, Block48 key) {
		Block32 res = new Block32();
		Block48 extraBlock = new Block48();
		expansionPermutate(extraBlock, src);
		xorKey(extraBlock, key);
		diffuse(res, extraBlock);
		straightPermutate(res);
		return res;
	}

	private static void xorBlock(Block32 b1, final Block32 b2) {
		for (int i = 0; i < b1.data.length; i++) {
			b1.data[i] ^= b2.data[i];
		}
	}

	private static void xorKey(Block48 block, final Block48 key) {
		for (int i = 0; i < block.data.length; i++) {
			block.data[i] ^= key.data[i];
		}
	}

	private static void expansionPermutate(Block48 dst, Block32 src) {
		dst.data = permutate(src.data, expansionPermutationTable);
	}

	private static void diffuse(Block32 dst, Block48 src) {
		byte[] ss = splitTetra(src);
		byte[] rr = substitute(ss);
		dst.data = combineTetra(rr);
	}

	private static byte[] splitTetra(Block48 block) {
		byte[] res = new byte[8];
		byte[] s = block.data;
		res[0] = (byte) ((s[0] >> 2) & 0x3f);
		res[1] = (byte) (((s[0] & 0x03) << 4) | ((s[1] & 0xf0) >> 4));
		res[2] = (byte) (((s[1] & 0x0f) << 2) | ((s[2] & 0x30) >> 6));
		res[3] = (byte) (s[2] & 0x3f);
		res[4] = (byte) ((s[3] >> 2) & 0x3f);
		res[5] = (byte) (((s[3] & 0x03) << 4) | ((s[4] & 0xf0) >> 4));
		res[6] = (byte) (((s[4] & 0x0f) << 2) | ((s[5] & 0x30) >> 6));
		res[7] = (byte) (s[5] & 0x3f);
		return res;
	}

	private static byte[] substitute(byte[] src) {
		byte[] res = new byte[8];
		for (int i = 0; i < 8; i++) {
			byte row = (byte) (((src[0] & 0x20) >> 4) | (src[0] & 0x01));
			byte col = (byte) ((src[0] & 0x1e) >> 1);
			res[i] = substitutionTable[i][row][col];
		}
		return res;
	}

	private static byte[] combineTetra(byte[] src) {
		byte[] res = new byte[4];
		res[0] = (byte) (((src[0] & 0x0f) << 4) | (src[1] & 0x0f));
		res[1] = (byte) (((src[2] & 0x0f) << 4) | (src[3] & 0x0f));
		res[2] = (byte) (((src[4] & 0x0f) << 4) | (src[5] & 0x0f));
		res[3] = (byte) (((src[6] & 0x0f) << 4) | (src[7] & 0x0f));
		return res;
	}

	private static void straightPermutate(Block32 p) {
		p.data = permutate(p.data, straightPermutationTable);
	}

	static byte[] initailPermutationTable = {
		58, 50, 42, 34, 26, 18, 10, 2,
		60, 52, 44, 36, 28, 20, 12, 4,
		62, 54, 46, 38, 30, 22, 14, 6,
		64, 56, 48, 40, 32, 24, 16, 8,
		57, 49, 41, 33, 25, 17, 9, 1,
		59, 51, 43, 35, 27, 19, 11, 3,
		61, 53, 45, 37, 29, 21, 13, 5,
		63, 55, 47, 39, 31, 23, 15, 7
	};
	static byte[] finalPermutationTable = {
		40, 8, 48, 16, 56, 24, 64, 32,
		39, 7, 47, 15, 55, 23, 63, 31,
		38, 6, 46, 14, 54, 22, 62, 30,
		37, 5, 45, 13, 53, 21, 61, 29,
		36, 4, 44, 12, 52, 20, 60, 28,
		35, 3, 43, 11, 51, 19, 59, 27,
		34, 2, 42, 10, 50, 18, 58, 26,
		33, 1, 41, 9, 49, 17, 57, 25
	};
	static byte[] expansionPermutationTable = {
		32, 1, 2, 3, 4, 5,
		4, 5, 6, 7, 8, 9,
		8, 9, 10, 11, 12, 13,
		12, 13, 14, 15, 16, 17,
		16, 17, 18, 19, 20, 21,
		20, 21, 22, 23, 24, 25,
		24, 25, 26, 27, 28, 29,
		28, 29, 31, 31, 32, 1
	};
	static byte[] straightPermutationTable = {
		16, 7, 20, 21, 29, 12, 28, 17,
		1, 15, 23, 26, 5, 18, 31, 10,
		2, 8, 24, 14, 32, 27, 3, 9,
		19, 13, 30, 6, 22, 11, 4, 25
	};
	static byte[][][] substitutionTable = {
		{
			{14, 4, 13, 1, 2, 15, 11, 8, 3, 10, 6, 12, 5, 9, 0, 7},
			{0, 15, 7, 4, 14, 2, 13, 1, 10, 6, 12, 11, 9, 5, 3, 8},
			{4, 1, 14, 8, 13, 6, 2, 11, 15, 12, 9, 7, 3, 10, 5, 0},
			{15, 12, 8, 2, 4, 9, 1, 7, 5, 11, 3, 14, 10, 0, 6, 13}
		},
		{
			{15, 1, 8, 14, 6, 11, 3, 4, 9, 7, 2, 13, 12, 0, 5, 10},
			{3, 13, 4, 7, 15, 2, 8, 14, 12, 0, 1, 10, 6, 9, 11, 5},
			{0, 14, 7, 11, 10, 4, 13, 1, 5, 8, 12, 6, 9, 3, 2, 15},
			{13, 8, 10, 1, 3, 15, 4, 2, 11, 6, 7, 12, 0, 5, 14, 9}
		},
		{
			{10, 0, 9, 14, 6, 3, 15, 5, 1, 13, 12, 7, 11, 4, 2, 8},
			{13, 7, 0, 9, 3, 4, 6, 10, 2, 8, 5, 14, 12, 11, 15, 1},
			{13, 6, 4, 9, 8, 15, 3, 0, 11, 1, 2, 12, 5, 10, 14, 7},
			{1, 10, 13, 0, 6, 9, 8, 7, 4, 15, 14, 3, 11, 5, 2, 12}
		},
		{
			{7, 13, 14, 3, 0, 6, 9, 10, 1, 2, 8, 5, 11, 12, 4, 15},
			{13, 8, 11, 5, 6, 15, 0, 3, 4, 7, 2, 12, 1, 10, 14, 9},
			{10, 6, 9, 0, 12, 11, 7, 13, 15, 1, 3, 14, 5, 2, 8, 4},
			{3, 15, 0, 6, 10, 1, 13, 8, 9, 4, 5, 11, 12, 7, 2, 14}
		},
		{
			{2, 12, 4, 1, 7, 10, 11, 6, 8, 5, 3, 15, 13, 0, 14, 9},
			{14, 11, 2, 12, 4, 7, 13, 1, 5, 0, 15, 10, 3, 9, 8, 6},
			{4, 2, 1, 11, 10, 13, 7, 8, 15, 9, 12, 5, 6, 3, 0, 14},
			{11, 8, 12, 7, 1, 14, 2, 13, 6, 15, 0, 9, 10, 4, 5, 3}
		},
		{
			{12, 1, 10, 15, 9, 2, 6, 8, 0, 13, 3, 4, 14, 7, 5, 11},
			{10, 15, 4, 2, 7, 12, 9, 5, 6, 1, 13, 14, 0, 11, 3, 8},
			{9, 14, 15, 5, 2, 8, 12, 3, 7, 0, 4, 10, 1, 13, 11, 6},
			{4, 3, 2, 12, 9, 5, 15, 10, 11, 14, 1, 7, 6, 0, 8, 13}
		},
		{
			{4, 11, 2, 14, 15, 0, 8, 13, 3, 12, 9, 7, 5, 10, 6, 1},
			{13, 0, 11, 7, 4, 9, 1, 10, 14, 3, 5, 12, 2, 15, 8, 6},
			{1, 4, 11, 13, 12, 3, 7, 14, 10, 15, 6, 8, 0, 5, 9, 2},
			{6, 11, 13, 8, 1, 4, 10, 7, 9, 5, 0, 15, 14, 2, 3, 12}
		},
		{
			{13, 2, 8, 4, 6, 15, 11, 1, 10, 9, 3, 14, 5, 0, 12, 7},
			{1, 15, 13, 8, 10, 3, 7, 4, 12, 5, 6, 11, 0, 14, 9, 2},
			{7, 11, 4, 1, 9, 12, 14, 2, 0, 6, 10, 13, 15, 3, 5, 8},
			{2, 1, 14, 7, 4, 10, 8, 13, 15, 12, 9, 0, 3, 5, 6, 11}
		}
	};
}
